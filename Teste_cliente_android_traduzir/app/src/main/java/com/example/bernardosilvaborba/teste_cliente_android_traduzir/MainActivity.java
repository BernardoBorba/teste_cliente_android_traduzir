package com.example.bernardosilvaborba.teste_cliente_android_traduzir;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.ExecutionException;


public class MainActivity extends AppCompatActivity {



    Spinner spinner;
    Spinner spinner1;
    RadioButton r1,r2;
    TextView texto1,texto2;
    String textoasda;
    int escolhida=0;
    int escolhidato=0;
    String sentence;
    String sentence1;
    String modifiedSentence;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner = (Spinner) findViewById(R.id.spinner);
        spinner1 = (Spinner) findViewById(R.id.spinner2);
        texto1 = (TextView) findViewById(R.id.textView4);
        texto2 = (TextView) findViewById(R.id.textView6);
         textoasda=spinner.getSelectedItem().toString();
    }

    public void getfromserver(View v){
        System.out.println("ONCLICK");

        escolhida = spinner.getSelectedItemPosition();
        escolhidato= spinner1.getSelectedItemPosition();
        sentence = Integer.valueOf(escolhida).toString();
        sentence1 = Integer.valueOf(escolhidato).toString();
        AsyncTask<String, String, String> con = new executeTranslate().execute(String.valueOf(spinner1.getSelectedItemPosition()),String.valueOf(spinner.getSelectedItemPosition()));
        String Recebido=null;
        try {
            Recebido = con.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        texto1.setText(spinner.getSelectedItem().toString());
        texto2.setText(Recebido);
    }

    private class executeTranslate extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String receiveSTR = null;
                try {
                    //BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
                    Socket clientSocket = new Socket("192.168.132.52", 6788);
                    DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
                    BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    sentence = Integer.valueOf(escolhida).toString();
                    sentence1 = Integer.valueOf(escolhidato).toString();
                    System.out.println(sentence);
                    outToServer.writeBytes(sentence1+sentence+'\n');
                    modifiedSentence = inFromServer.readLine();
                    System.out.println("FROM SERVER: " + modifiedSentence);
                    clientSocket.close();
                    return modifiedSentence;
                } catch (IOException iOException) {
                    System.out.println("erro");
                }
            return null;
        }
    }
}
